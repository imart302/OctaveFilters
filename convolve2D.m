function o = convolve2D(I, ker)


kflip = flip(ker);
kflip = flip(kflip, 2);

[M,N,C] = size(I);
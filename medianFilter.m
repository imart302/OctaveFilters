function Iout = medianFilter(I, k)

V = zeros(k*k, 1);

[M,N,C] = size(I)

Ofm = M - 2*floor(k/2)
Ofn = N - 2*floor(k/2)

Out = zeros(Ofm, Ofn, C);

for c = 1:C
    for row = 1:Ofm
        for col = 1:Ofn
            count = 1;
            for i = 0:k-1
                for j = 0:k-1
                    %count
                    V(count) = I(row+i, col+j, c);
                    count = count + 1;
                endfor

            endfor

            Out(row, col, c) = median(V);

        endfor

    endfor

endfor

Iout = Out;
endfunction